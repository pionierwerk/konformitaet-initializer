# KonFormität game template
this is a initializer to get a working [KonFormität](https://gitlab.com/pionierwerk/konformitaet) based project.

The project includes a fully working game that offers:
- Single player game
- Multi player game

- A logo screen
- A menu screen
- A game screen
- A high score name enter screen
- A high score table screen

- Game parameters changeable in the menu
- Credits screen in the menu
- How to play screen in the menu
- High score table viewer screen in the menu
- Toggle between fullscreen and windowed mode in the menu
- Quit game in the menu

- High score database with tables that depend on the choosen options

- The complete game is controlable either by keyboard or by joystick

- Winning sounds, Loosing sounds
- Background music

# Getting started
- [Install Lua](https://www.lua.org/download.html)
- [Install Love2D](https://love2d.org/)
- Download the Konformität-installer

	``` bash
	wget https://gitlab.com/pionierwerk/konformitaet-initializer/-/archive/master/konformitaet-initializer-master.zip
	```
- unzip the initializer

	``` bash
	unzip konformitaet-initializer-master.zip
	```

- use the initializer to create a new project. Replace new_project_name with the name of your new project

	``` bash
	cd konformitaet-initializer-master
	./konformitaet-initializer.sh new_project_name
	```

- start the new Konformität bases project. Replace new_project_name with the name of your new project

	``` bash
	cd new_project_name
	love .
	```
# ----------------------------------------------
# Konformität Template initializer
# ----------------------------------------------
# setup a Konformität project with minimal setup
# ----------------------------------------------
# licenced und zlib licence
# ----------------------------------------------
# þorN for pionierwerk.eu
# ----------------------------------------------

# get project name from the arguments or exit
# ----------------------------------------------
if [ "$#" -ne 1 ]; then
  	echo "Usage: $0 PROJECT_NAME" >&2
	exit -1
else
	project_name=$1
fi

# set variables
# -------------
base_dir=`pwd`
template_file=konformitaet-game-template-master
template_zip_file=$template_file.zip
template_src=https://gitlab.com/pionierwerk/konformitaet-game-template/-/archive/master
temp_folder=temp
lib_folder=lib

# create project directory
# ----------------------------------------------
mkdir $project_name								# create directory
cd $project_name								# change into project directory

# initialize git
# ----------------------------------------------
git init

# get template zip file
wget $template_src/$template_zip_file			# get Konformität zip file

unzip $template_file							# unzip zip file
mv $template_file $temp_folder					# rename zip to temp
cd $temp_folder/$lib_folder						# cd temp folder

# add submodule binser
# ----------------------------------------------
git submodule add https://github.com/bakpakin/binser.git

# add submodule dkjson
# ----------------------------------------------
git submodule add https://github.com/LuaDist/dkjson.git

# add submodule cargo
# ----------------------------------------------
git submodule add https://github.com/bjornbytes/cargo.git

# add submodule numberlua
# ----------------------------------------------
git submodule add https://github.com/davidm/lua-bit-numberlua.git

# add submodule KonFormitaet
# ----------------------------------------------
git submodule add https://gitlab.com/pionierwerk/konformitaet.git

# move all file into the root of the new project
# ----------------------------------------------
cd "$base_dir/$project_name/$temp_folder"		# cd temp
mv * "$base_dir/$project_name"					# move everything into the root
rm "$base_dir/$project_name/$template_zip_file"	# rm template zip file
rm -rf "$base_dir/$project_name/$temp_folder"	# rm temp

# return a "ok" value
# ----------------------------------------------
exit 0